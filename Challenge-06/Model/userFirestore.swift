//
//  userFirestore.swift
//  Challenge-06
//
//  Created by Tatang Sulaeman on 13/05/22.
//

import Foundation
import UIKit
import FirebaseFirestore

extension appUser {
    static func build(from documents: [QueryDocumentSnapshot]) -> [appUser] {
        var users = [appUser]()
        for document in documents {
            users.append(appUser(name: document["name"] as? String ?? "", lastName: document["lastname"] as? String ?? ""))
        }
        
        return users
    }
}
