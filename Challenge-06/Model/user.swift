//
//  user.swift
//  Challenge-06
//
//  Created by Tatang Sulaeman on 13/05/22.
//

import Foundation
import UIKit
import Firebase
import FirebaseCore
import FirebaseFirestore

struct appUser {
    let name: String?
    let lastName: String?
}
