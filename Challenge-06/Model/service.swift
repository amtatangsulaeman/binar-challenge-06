//
//  service.swift
//  Challenge-06
//
//  Created by Tatang Sulaeman on 13/05/22.
//

import Foundation
import UIKit
import FirebaseFirestore

class service {
    let database = Firestore.firestore()
    
    func get (collectionID: String, handler: @escaping ([appUser]) -> Void) {
        database.collection("User")
            .addSnapshotListener { querySnapshot, err in
                if let error = err {
                    print(error)
                    handler([])
                } else {
                    handler(appUser.build(from: querySnapshot?.documents ?? []))
                }
                
            }
    }
}
