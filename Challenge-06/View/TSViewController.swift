//
//  TSViewController.swift
//  Challenge-06
//
//  Created by Tatang Sulaeman on 13/05/22.
//

import UIKit
import FirebaseFirestore

class TSViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    private var userService: service?
    private var allusers = [appUser]() {
        didSet {
            DispatchQueue.main.async {
                self.users = self.allusers
            }
        }
    }
    
    var users = [appUser](){
        didSet {
            DispatchQueue.main.async {
                self.TSTableView.reloadData()
            }
        }
    }
    let TSTableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        TSTableView.delegate = self
        
        
        setupTable()
        // Do any additional setup after loading the view.
        
        
    }
    
    func load(){
        userService = service()
        userService?.get(collectionID: "User") { users in
            self.allusers = users
        }
    }
    
    func setupTable() {
        view.addSubview(TSTableView)
        TSTableView.allowsSelection = true
        TSTableView.isUserInteractionEnabled = true
        TSTableView.translatesAutoresizingMaskIntoConstraints = false
        TSTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        TSTableView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        TSTableView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true
        TSTableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        TSTableView.dataSource = self
        TSTableView.register(UITableViewCell.self, forCellReuseIdentifier: "TSCell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "TSCell")
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = users[indexPath.row].name
        cell.textLabel?.font = .systemFont(ofSize: 20, weight: .medium)
        cell.detailTextLabel?.text = users[indexPath.row].lastName
        return cell
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

